import { queryProducts } from "./API.js";
import { renderProductList } from "./products.js";

export const catalog = document.getElementsByClassName("catalog")[0];

// sort fill
const fillCatalog = async (sort) => {
  const products = await queryProducts(sort);
  if (sort === "asc") {
    const sorted = products.sort((a, b) => b.price - a.price);
    catalog.innerHTML = renderProductList(sorted);
    return;
  } else if (sort === "desc") {
    const sorted = products.sort((a, b) => a.price - b.price);
    catalog.innerHTML = renderProductList(sorted);
    return;
  } else {
    catalog.innerHTML = renderProductList(products);
  }
};

fillCatalog();

// sort
const sort = document.getElementById("sort");
sort.addEventListener("change", () => {
  fillCatalog(sort.value);
});

// search
const searchQuery = document.getElementById("searchQuery");
const searchButton = document.getElementById("searchButton");

searchButton.addEventListener("click", async () => {
  const products = await queryProducts();
  const result = products.filter((item) =>
    item.title.toLowerCase().includes(searchQuery.value.toLowerCase())
  );
  catalog.innerHTML = renderProductList(result);
});
