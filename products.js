export const renderProduct = (product) => `
<div class="catalog__product">
<div class="catalog__head">
<label class="catalog__checkbox-container">
    <input type="checkbox">
    <span class="catalog__checkmark"></span>
</label>
<button class="catalog__inventorybtn">add to inventory</button>
</div>
<div class="catalog__img">
    <img src ="${product.image}">
</div>
<div class="catalog__title">
${product.title}
</div>
<div class="catalog__prices">
    ${product.price}$
</div>
</div>
`;

export const renderProductList = (allProducts) => {
    let productsContent = '';
    for (const product of allProducts) {
        productsContent += renderProduct(product);
    }
    return productsContent;
};