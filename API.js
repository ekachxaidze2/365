import {
    API_URL
} from "./config.js";

const call = async (url) => {
    const request = await fetch(API_URL + url)
    return await request.json();
}

export const queryProducts = async (sort = null) =>
    await call(`products${sort ? `?sort=${sort}`: ''}`);


export const categories = async () =>
    await call("products/categories")